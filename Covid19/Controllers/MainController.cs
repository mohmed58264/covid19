﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Covid19.Data;
using Covid19.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Covid19.Controllers
{
    [Produces("application/json")]
    [Route("api/Main/[Action]")]
    public class MainController : Controller
    {

        private readonly ApplicationDbContext _context;
        public MainController(ApplicationDbContext Context)
        {
            _context = Context;
        }

        [HttpGet]
        public IActionResult Login(string username, string password)
        {
            var user = _context.Shops.SingleOrDefault(m => m.Username == username && m.Password == password);
            if (user == null)
            {
                return Ok("0");
            }
            else
            {
                return Ok("1");
            }
        }



        [HttpPost]
        public IActionResult Register([FromBody] Shops shops)
        {
            if (ModelState.IsValid && _context.Shops.SingleOrDefault(m=>m.Username == shops.Username).Username == null)
            {
                _context.Add(shops);
                _context.SaveChanges();
            return Ok("1");
            }
            else
            {
                return Ok("0");
            }
        }

        [HttpPost]
        public IActionResult Update([FromBody] Shops shops)
        {
            if (ModelState.IsValid)
            {
                _context.Update(shops);
                _context.SaveChanges();
                return Ok("1");

            }else
            {
            return Ok("0");
            }
        }



        [HttpGet]
        public IActionResult GetAllShops(int type)
        {
            var shops = _context.Shops.Where(m => m.type == type).ToList();

            return Ok(shops);

        }



        [HttpGet]
        public IActionResult GetData(string username)
        {
            var shops = _context.Shops.SingleOrDefault(m => m.Username == username);

            return Ok(shops);

        }
    }
}