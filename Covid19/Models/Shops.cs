﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Covid19.Models
{
    public class Shops
    {
        public int Id { get; set; }
        public string ShopName { get; set; }
        public int AllCapacity { get; set; }
        public int CurrentCapacity { get; set; }     
        public int type { get; set; }
        public string ShopImage { get; set; }   
        public string Username { get; set; }   
        public string Password { get; set; }    
        public string Statuse { get; set; }       

    }
}
